(function(){

	console.log('checkSystemRequirements');
	console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

    // it's option if you want to change the WebSDK dependency link resources. setZoomJSLib must be run at first
    // if (!china) ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.6/lib', '/av'); // CDN version default
    // else ZoomMtg.setZoomJSLib('https://jssdk.zoomus.cn/1.7.6/lib', '/av'); // china cdn option
    // ZoomMtg.setZoomJSLib('http://localhost:9999/node_modules/@zoomus/websdk/dist/lib', '/av'); // Local version default, Angular Project change to use cdn version
    ZoomMtg.preLoadWasm();
    ZoomMtg.prepareJssdk();

    var API_KEY = '0wWKd4AZRkafuMCv9eeMEg';
    let appointmentId = $('#re-call').data('insider');
    let userType = $('#re-call').data('type');

    $(document).ready(function(){

        fetch(`/${userType}_dashboard/join_appointment_get_signature`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": $('input[name="_token"]').val()
            },
            body: JSON.stringify({appointmentId: appointmentId})
        })
            .then(result => result.text())
            .then(response => {
                let meetConfig = JSON.parse(response);
                console.log(meetConfig);
                ZoomMtg.init({
                    leaveUrl: 'http://www.google.us',
                    isSupportAV: true,
                    success: function () {
                        ZoomMtg.join(
                            {
                                meetingNumber: parseInt(meetConfig.meetingNumber),
                                userName: meetConfig.userName,
                                signature: meetConfig.signature,
                                apiKey: meetConfig.apiKey,
                                passWord: '123321',
                                success: function(res){
                                    $('#nav-tool').hide();
                                    console.log('join meeting success');
                                },
                                error: function(res) {
                                    console.log(res);
                                }
                            }
                        );
                    },
                    error: function(res) {
                        console.log('here');
                        console.log(res);
                    }
                });

            });



    });



})();
