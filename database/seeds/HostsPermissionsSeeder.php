<?php

use Illuminate\Database\Seeder;

class HostsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PermissionSeeder())->hosts();
    }
}
