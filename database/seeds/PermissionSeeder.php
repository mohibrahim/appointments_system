<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->permissions();
        $this->users();
        $this->roles();
        $this->profiles();
        $this->hosts();
        $this->appointments();
    }

    private function permissions()
    {
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'store_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'show_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'show_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'index_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'index_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'update_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'update_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'destroy_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'destroy_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    private function users()
    {
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'store_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'show_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'show_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'update_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'update_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'destroy_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'destroy_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users_not_verified_email',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'verify_user_email',
            'created_at' => now(),
        ]);
    }

    private function roles()
    {
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'store_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'show_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'show_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'index_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'index_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'update_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'update_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'destroy_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'destroy_roles_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    private function profiles()
    {
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'store_profiles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'show_profiles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'show_profiles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'index_profiles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'index_profiles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'update_profiles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'update_profiles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'destroy_profiles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'profiles',
            'name' => 'destroy_profiles_multi_tenancy',
            'created_at' => now(),
        ]);
    }
    public function hosts()
    {
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'store_hosts',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'show_hosts',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'show_hosts_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'index_hosts',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'index_hosts_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'update_hosts',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'update_hosts_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'destroy_hosts',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'hosts',
            'name' => 'destroy_hosts_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function appointments()
    {
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'store_appointments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'show_appointments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'show_appointments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'index_appointments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'index_appointments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'update_appointments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'update_appointments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'destroy_appointments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'appointments',
            'name' => 'destroy_appointments_multi_tenancy',
            'created_at' => now(),
        ]);
    }
}
