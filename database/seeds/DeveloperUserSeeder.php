<?php

use App\Role;
use App\User;
use App\Permission;
use App\Services\Models\User\Type\UserType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeveloperUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moh = new User();
        $moh->type = UserType::OTHER;
        $moh->first_name = "Mohammed";
        $moh->last_name = "Ibrahim";
        $moh->email = "mohibrahimqop@gmail.com";
        $moh->password = bcrypt("password");
        $moh->email_verified_at = now();
        $moh->creator_id = 1;
        $moh->save();

        $roleDeveloper = new Role();
        $roleDeveloper->name = "Developer";
        $roleDeveloper->save();

        $moh->update(['master_role_id' => $roleDeveloper->id]);

        $moh->roles()->attach($roleDeveloper);

        $permissions = Permission::all();
        $roleDeveloper->permissions()->attach($permissions->pluck('id'));

        // ---------------------------------------
        $roleAdmin = new Role();
        $roleAdmin->name = UserType::ADMIN;
        $roleAdmin->save();

        $adminPermissions = Permission::where('title', 'hosts')
            ->get();
        $roleAdmin->permissions()->attach($adminPermissions->pluck('id'));

        $adminPermissions = Permission::where('title', 'appointments')
            ->get();
        $roleAdmin->permissions()->attach($adminPermissions->pluck('id'));
        // ---------------------------------------

        $roleHost = new Role();
        $roleHost->name = UserType::HOST;
        $roleHost->save();

        // ---------------------------------------
        $roleAttendee = new Role();
        $roleAttendee->name = UserType::ATTENDEE;
        $roleAttendee->save();

        // ---------------------------------------




        DB::table('permissions')->update(['creator_id' => 1]);
        DB::table('roles')->update(['creator_id' => 1]);
    }
}
