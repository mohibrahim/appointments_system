<?php

use Illuminate\Database\Seeder;

class AppointmentsPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PermissionSeeder())->appointments();
    }
}
