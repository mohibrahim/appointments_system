<?php

return [
    // Authorization
    'authorizations'=>'Authorizations',
    'users'=>'Users',
    'create_users' => 'Add new user',
    'index_users' => 'All users',

    'profiles'=>'Profiles',
    'create_profiles' => 'Add new profile',
    'index_profiles' => 'All profiles',

    'permissions'=>'Permissions',
    'create_permissions' => 'Add new permission',
    'index_permissions' => 'All permissions',

    'roles'=>'Roles',
    'create_roles' => 'Add new role',
    'index_roles' => 'All roles',
    'log_out' => 'Log out',

    'hosts'=>'Hosts',
    'create_hosts' => 'Add new host',
    'index_hosts' => 'All hosts',

    'appointments'=>'Appointments',
    'create_appointments' => 'Add new appointment',
    'index_appointments' => 'All appointments',

    //Navbar
    'language' => 'Language',
];
