<?php

return [
    'confirm_delete'=>'Confirm Delete',
    'warning'=>'Warning!',
    'close'=>'Close',
    'delete'=>'Delete',
];