<?php
return [
    'appointment'=>'Appointment',
    'all_appointments'=>'Appointments',
    'edit_appointment' => 'Edit appointment',

    'add_appointment'=>'Add Appointment',
    'id' => 'ID',
    'host_id' => 'Host name',
    'host_id_placeholder' => 'Select Host Name',
    'attendee_id' => 'Attendee name',
    'attendee_id_placeholder' => 'Select Attendee Name',
    'title' => 'Title',
    'title_placeholder' => 'Enter full title',
    'appointment_start' => 'Start',
    'appointment_start_placeholder' => 'Enter the Start date time',
    'appointment_end' => 'End',
    'appointment_end_placeholder' => 'Enter the End date time',
    'notes' => 'Notes',
    'notes_placeholder' => 'Enter your notes',
    'save' => 'Save',
    'confirm_delete_message' => 'Are you sure you want to delete the appointment of ',
    'searching_on_appointment' => 'Searching on appointment',
    'searching_word' => 'Searching word',
    'search' => 'Search',
    'searching_button_searching' => 'Searching... ',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word to search for ...',

    'edit'=>'Edit',
    'delete'=>'Delete',
    //Flash messages
    'appointment_added_successfully' => 'Appointment Added Successfully.',
    'appointment_updated_successfully' => 'Appointment has been updated successfully.',
    'appointment_deleted_successfully_segment_1' => 'Appointment: ',
    'appointment_deleted_successfully_segment_2' => 'has been deleted successfully.',

    //Validation

];
