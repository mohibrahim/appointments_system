<?php
return [
    'host'=>'Host',
    'all_hosts'=>'Hosts',
    'edit_host' => 'Edit host',

    'add_host'=>'Add Host',
    'name' => 'Name',
    'name_placeholder' => 'Enter full name',
    'email' => 'E-Mail',
    'email_placeholder' => 'Enter the E-mail',
    'password' => 'Password',
    'password_placeholder' => 'Enter the password',
    'password_confirmation' => 'Password confirmation',
    'password_confirmation_placeholder' => 'Enter password confirmation',
    'notes' => 'Notes',
    'notes_placeholder' => 'Enter your notes',
    'save' => 'Save',
    'confirm_delete_message' => 'Are you sure you want to delete the host of ',
    'searching_on_host' => 'Searching on host',
    'searching_word' => 'Searching word',
    'search' => 'Search',
    'searching_button_searching' => 'Searching... ',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word to search for ...',

    'edit'=>'Edit',
    'delete'=>'Delete',
    //Flash messages
    'host_added_successfully' => 'Host Added Successfully.',
    'host_updated_successfully' => 'Host has been updated successfully.',
    'host_deleted_successfully_segment_1' => 'Host: ',
    'host_deleted_successfully_segment_2' => 'has been deleted successfully.',

    //Validation

];
