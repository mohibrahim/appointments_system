<?php
return [
    'profile'=>'Profile',
    'all_profiles'=>'Profiles',
    'edit_profile' => 'Edit profile',

    'add_profile'=>'Add profile',
    'user_id' => 'User Name',
    'user_id_placeholder' => 'Select user name.',
    'date_of_birth' => 'Date of birth',
    'date_of_birth_placeholder' => 'Select date of birth',
    'phone' => 'Mobile number',
    'phone_placeholder' => 'Enter mobile number',
    'id_card_number' => 'ID card number',
    'id_card_number_placeholder' => 'Enter ID card number',
    'country' => 'Country',
    'country_placeholder' => 'choose the country',
    'address' => 'Address',
    'address_placeholder' => 'Enter the address',
    'personal_image' => 'Personal image',
    'personal_image_placeholder' => 'Choose the personal image',
    'notes' => 'Notes',
    'notes_placeholder' => 'Enter your notes',
    'save' => 'Save',
    'confirm_delete_message' => 'Are you sure you want to delete the profile of ',
    'searching_on_profile' => 'Searching on profile',
    'searching_word' => 'Searching word',
    'search' => 'Search',
    'searching_button_searching' => 'Searching... ',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word to search for ...',

    'edit'=>'Edit',
    'delete'=>'Delete',
    //Flash messages
    'profile_added_successfully' => 'Profile Added Successfully.',
    'profile_updated_successfully' => 'Profile has been updated successfully.',
    'profile_deleted_successfully_segment_1' => 'Profile: ',
    'profile_deleted_successfully_segment_2' => 'has been deleted successfully.',

    //Validation

    'user_already_has_profile'=>' Selected user already has profile',


];
