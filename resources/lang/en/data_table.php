<?php

return [
    'search' => 'Search the exposed results: ',
    "zeroRecords" => "No data available in the table",
];
