isEmptyOrSpaces = function(str){
    return str === null || str.match(/^ *$/) !== null;
}


optional = function (arg1, arg2)
{
    if (arg1 !== null && !(typeof arg1 === 'undefined')) {
        return arg1[arg2];
    } else {
        return "";
    }
}

onlyOneClick = function () {
    $(".only-one-click").on ('click', function(){
        $(this).attr('disabled', 'disabled');
    });
}
