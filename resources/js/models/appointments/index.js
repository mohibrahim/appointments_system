searchingOnAppointmentsAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingButtonRequired = $("#searching-button-required");
    var searchingButtonSearch = $("#searching-button-search");
    var searchingButtonSearching = $("#searching-button-searching");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');
    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);

    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {

            $.ajax({
                type: 'POST',
                url: '/appointments_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val()
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, appointment) {
                        console.log(appointment);
                        dataTable.row.add([
                            key + 1,
                            appointment.id,
                            appointment.appointment_show_link,
                            appointment.appointment_start,
                            appointment.appointment_end,
                            appointment.created_at,
                            optional(appointment.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                },
                error: function(){
                    console.log('error');
                },
            });
        } else {
            searchingButton.text(searchingButtonRequired.val()).addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text(searchingButtonSearching.val()).fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text(searchingButtonSearch.val()).removeClass("btn-danger");
    }

}
