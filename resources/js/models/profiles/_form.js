deletePersonalImage = function () {
    //CACHE DOM
    let $deletePersonalImageButton = $('#delete-personal-image');
    let $personalImageWrapper = $('#personal-image-wrapper');
    let $navUserProfileImage = $('#user-profile-image');
 
    //BIND EVENTS
    $deletePersonalImageButton.on('click', deleteTheImage);
 
    //FUNCTION BODY
  
 
    //INNER FUNCTIONS
    
    function deleteTheImage(event) {
        $.ajax({
            type:"get",
            url:"/profiles_miscellaneous/delete_personal_image",
            data:{
                "personal_image_name": $(this).data("personal-image-name")
            },
            success: function(){
                $navUserProfileImage.attr('src', '/images/profiles/no_image.png')
                $personalImageWrapper.empty().fadeOut();
            },
        });
    }
}