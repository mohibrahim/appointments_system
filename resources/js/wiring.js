require('./models/users/index');
require('./models/profiles/_form');
require('./models/profiles/index');
require('./models/appointments/index');
require('./models/users/hosts/index');
//Overlay scrollbar
require('overlayscrollbars/js/OverlayScrollbars');
//Admin lte
require('admin-lte');
require('./system');
//Select2
require('select2/dist/js/select2.full.min');


