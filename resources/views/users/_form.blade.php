<div class="row">

	<div class="col-lg-12">

		@if (in_array('store_roles', $permissions) || in_array('update_roles', $permissions))
			<div class='form-group'>
				{!!Form::label('master_role_id', __('user.master_role_id'))!!}
				{!!Form::select('master_role_id', $user->user_roles_names_ids, null,['class'=>'form-control', 'placeholder'=>__('user.master_role_id_placeholder')]) !!}
			</div>
        @endif
        <div class='form-group'>
            <span style='color:#f47373'>*</span>
            {!!Form::label('type', __('user.type'))!!}
            {!!Form::select('type', $user->types, null,['class'=>'form-control', 'placeholder'=>__('user.type_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('first_name', __('user.first_name'))!!}
			{!!Form::text('first_name', null,['class'=>'form-control', 'placeholder'=>__('user.first_name_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('last_name', __('user.last_name'))!!}
			{!!Form::text('last_name', null,['class'=>'form-control', 'placeholder'=>__('user.last_name_placeholder')]) !!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('email', __('user.email'))!!}
			{!!Form::email('email', null, ['class'=>'form-control', 'placeholder'=>__('user.email_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password', __('user.password'))!!}
			{!!Form::password('password', ['class'=>'form-control', 'placeholder'=>__('user.password_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password_confirmation', __('user.password_confirmation'))!!}
			{!!Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>__('user.password_confirmation_placeholder')])!!}
		</div>


		<div class='form-group'>
			{!!Form::label('notes', __('user.notes'))!!}
			{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('user.notes_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::submit(__('user.save'), ['class'=>'btn btn-primary form-control'])!!}
		</div>
	</div>
</div>
