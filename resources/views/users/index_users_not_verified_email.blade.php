@extends('layouts.app')
@section('title')
    المستخدمين الذين لم التحقق من البريد الإلكتروني الخاص بهم
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				المستخدمين الذين لم التحقق من البريد الإلكتروني الخاص بهم
			</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover" id="owners">
							<thead>
								<tr>
									<th class="border-0">#</th>
									<th class="border-0">الاسم</th>
									<th class="border-0">البريد الإلكتروني</th>
									<th class="border-0">رقم المحمول </th>
									<th class="border-0">البلد </th>
									<th class="border-0">تاريخ تاريخ الاشتراك </th>
									<th class="border-0">نوع الاشتراك </th>
									<th class="border-0">تفعيل الاشتراك </th>
								</tr>
							</thead>
							<tbody>
								@foreach($usersNotVerified as $userKey => $user)
									<tr>
										<td>{{$userKey+1}}</td>
										<td>
                                            {{$user->name}}
										</td>
										<td>{{$user->email}}</td>
										<td>{{$user->mobile_number}}</td>
										<td>{{$user->country}}</td>
										<td>{{$user->created_at}}</td>
										<td>{{$user->type}}</td>
										<td>
                                            <a href="{{action('UserController@verifyUserEmail', ['user'=>$user->id])}}" role="button" class="btn btn-primary btn-sm">تفعيل</a>
                                        </td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>

				</div>
				<div class="d-flex justify-content-center">
					{{$usersNotVerified->links()}}
				</div>
			</div>
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#owners').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": "البحث في النتائج المعروضه: ",
					 "zeroRecords": "لا يوجد بيانات متاحة في الجدول",
				},
			});
		});
	</script>
@endsection