@extends('layouts.app')

@section('title')
	{{$user->name}}
@endsection
@section('content-name')
    {{$user->name}}
@endsection
@section('content')
    <div class="card">
        <div class="card-body ">
            <table class="table">
                <tbody>
                    <tr>
                        <th class="border-top-0">Type</th>
                        <td class="border-top-0" scop="row">{{$user->type}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Master Role Name</th>
                        <td class="border-top-0" scop="row">{{optional($user->masterRole)->name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">ID</th>
                        <td class="border-top-0" scop="row">{{$user->id}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Personal Image</th>
                        <td class="border-top-0" scop="row">
                            <img src="{{asset('project_files/'.optional($user->personalImage)->name)}}" class="img img-fluid rounded-circle" alt="" width="200">
                        </td>
                    </tr>
                    <tr>
                        <th class="border-top-0">First Name</th>
                        <td class="border-top-0" scop="row"> {{$user->first_name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Last Name</th>
                        <td class="border-top-0" scop="row"> {{$user->last_name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Email</th>
                        <td class="border-top-0" scope="row"> {{ $user->email}} </td>
                    </tr>
                    <tr>
                        <th class="border-top-0">E-Mail verification at</th>
                        <td class="border-top-0" scope="row"> {{ $user->email_verified_at}} </td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Role</th>
                        <td class="border-top-0" scope="row">
                            @if($user->roles->isNotEmpty())
                                @foreach ($user->roles as $key => $role)
                                    {{ $role->name}}<br>
                                @endforeach
                            @else
                                No Role Yet!
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <th class="border-top-0">Notes</th>
                        <td class="border-top-0" scope="row"> {{$user->notes}} </td>
                    </tr>
                    <tr>
                        @if(in_array('update_users', $permissions))
                            <th class="border-top-0">Assign role</th>
                            <td class="border-top-0" scope="row"> <a href="{{ action('RoleUserController@edit', ['role_user'=>$user->id]) }}"> Edit </td>
                        @endif
                    </tr>
                    <tr>
                        @if(in_array('update_users', $permissions))
                            <th class="border-top-0">Edit</th>
                            <td class="border-top-0" scope="row"> <a href="{{action('UserController@edit',['user'=>$user->id])}}">Eidt</a> </td>
                        @endif
                    </tr>
                    <tr>
                        @if(in_array('destroy_users', $permissions))
                            <th class="border-top-0">Delete</th>
                            <td class="border-top-0" scope="row"><button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> Delete</button></td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


	@include('partial.deleteConfirm',['name'=>$user->name,
									  'message'=> __('user.confirm_delete_message'),
									  'route'=> action('UserController@destroy', ['user'=>$user->id])])
@endsection
