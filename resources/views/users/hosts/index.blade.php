@extends('layouts.app')
@section('title')
	{{__('host.all_hosts')}}
@endsection
@section('content-name')
    {{__('host.all_hosts')}}
@endsection
@section('content')
		<div class="card">

				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('host.searching_on_host')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('host.searching_word')}}" autofocus>
						</div>

						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('host.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('host.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('host.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('host.searching_button_search')}}">
					</div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover" id="hosts">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>created at</th>
                                    <th>Creator name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($hosts as $hostKey => $host)
                                    <tr>
                                        <td>{{$hostKey+1}}</td>
                                        <td>{{$host->id}}</td>
                                        <td>
                                            {!!$host->host_show_link!!}
                                        </td>

                                        <td>{{$host->email}}</td>
                                        <td>{{$host->created_at}}</td>
                                        <td>{{optional($host->creator)->name}}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
				</div>
				<div class="d-flex justify-content-center">
					{{$hosts->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">


@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#hosts').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnHostsAjax();
		});
	</script>
@endsection
