@extends('layouts.app')

@section('title')
	{{$host->name}}
@endsection
@section('content-name')
    {{$host->name}}
@endsection
@section('content')
    <div class="card">
        <div class="card-body ">
            <table class="table">
                <tbody>
                    <tr>
                        <th class="border-top-0">ID</th>
                        <td class="border-top-0" scop="row">{{$host->id}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Name</th>
                        <td class="border-top-0" scop="row"> {{$host->name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Email</th>
                        <td class="border-top-0" scope="row"> {{ $host->email}} </td>
                    </tr>


                    <tr>
                        <th class="border-top-0">Notes</th>
                        <td class="border-top-0" scope="row"> {{$host->notes}} </td>
                    </tr>
                   <tr>
                        @if(in_array('update_hosts', $permissions))
                            <th class="border-top-0">Edit</th>
                            <td class="border-top-0" scope="row"> <a href="{{action('HostController@edit',['host'=>$host->id])}}">Eidt</a> </td>
                        @endif
                    </tr>
                    <tr>
                        @if(in_array('destroy_hosts', $permissions))
                            <th class="border-top-0">Delete</th>
                            <td class="border-top-0" scope="row"><button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> Delete</button></td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


	@include('partial.deleteConfirm',['name'=>$host->name,
									  'message'=> __('host.confirm_delete_message'),
									  'route'=> action('HostController@destroy', ['host'=>$host->id])])
@endsection
