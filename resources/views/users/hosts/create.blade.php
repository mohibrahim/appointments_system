@extends('layouts.app')
@section('title')
	{{__('host.add_host')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{__('host.add_host')}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::open(['action' => ['HostController@store'], 'files'=>true, 'method'=>'POST'])!!}
					@include('users.hosts._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
