@extends('layouts.app')
@section('title')
	Edit Host
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{$host->name}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::model($host, ['action' => ['HostController@update', 'host'=>$host->id], 'files'=>true, 'method'=>'put'])!!}
					@include('users.hosts._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection

