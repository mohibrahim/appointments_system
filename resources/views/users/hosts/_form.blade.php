<div class="row">

	<div class="col-lg-12">

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('first_name', __('user.first_name'))!!}
			{!!Form::text('first_name', null,['class'=>'form-control', 'placeholder'=>__('user.first_name_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('last_name', __('user.last_name'))!!}
			{!!Form::text('last_name', null,['class'=>'form-control', 'placeholder'=>__('user.last_name_placeholder')]) !!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('email', __('host.email'))!!}
			{!!Form::email('email', null, ['class'=>'form-control', 'placeholder'=>__('host.email_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password', __('host.password'))!!}
			{!!Form::password('password', ['class'=>'form-control', 'placeholder'=>__('host.password_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password_confirmation', __('host.password_confirmation'))!!}
			{!!Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>__('host.password_confirmation_placeholder')])!!}
		</div>


		<div class='form-group'>
			{!!Form::label('notes', __('host.notes'))!!}
			{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('host.notes_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::submit(__('host.save'), ['class'=>'btn btn-primary form-control'])!!}
		</div>
	</div>
</div>
