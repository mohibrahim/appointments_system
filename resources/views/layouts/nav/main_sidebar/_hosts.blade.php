@if(in_array('index_hosts', $permissions) || in_array('store_hosts', $permissions) )

    <li class="nav-item has-treeview {{(session('highlighSelectedNav') == 'hosts')?('menu-open'):('')}}">
        <a href="#" class="nav-link {{(session('highlighSelectedNav') == 'hosts')?('active'):('')}}">
            <i class="fas fa-person-booth"></i>
            <p>
                {{__('nav.hosts')}}
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview ml-3">
            @if (in_array('store_hosts', $permissions))
                <li class="nav-item">
                    <a href="{{action('HostController@create')}}" class="nav-link">
                        <i class="far fa-plus-square"></i>
                        <p>{{__('nav.create_hosts')}}</p>
                    </a>
                </li>
            @endif
            @if (in_array('index_hosts', $permissions))
                <li class="nav-item">
                    <a href="{{action('HostController@index')}}" class="nav-link">
                        <i class="fas fa-list-ol"></i>
                        <p>{{__('nav.index_hosts')}}</p>
                    </a>
                </li>
            @endif
        </ul>
    </li>

@endif
