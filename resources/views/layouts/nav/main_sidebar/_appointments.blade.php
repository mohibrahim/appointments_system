@if(in_array('index_appointments', $permissions) || in_array('store_appointments', $permissions) )

    <li class="nav-item has-treeview {{(session('highlighSelectedNav') == 'appointments')?('menu-open'):('')}}">
        <a href="#" class="nav-link {{(session('highlighSelectedNav') == 'appointments')?('active'):('')}}">
            <i class="fas fa-calendar-check"></i>
            <p>
                {{__('nav.appointments')}}
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview  ml-3">
            @if (in_array('store_appointments', $permissions))
                <li class="nav-item">
                    <a href="{{action('AppointmentController@create')}}" class="nav-link">
                        <i class="far fa-plus-square"></i>
                        <p>{{__('nav.create_appointments')}}</p>
                    </a>
                </li>
            @endif
            @if (in_array('index_appointments', $permissions))
                <li class="nav-item">
                    <a href="{{action('AppointmentController@index')}}" class="nav-link">
                        <i class="fas fa-list-ol"></i>
                        <p>{{__('nav.index_appointments')}}</p>
                    </a>
                </li>
            @endif
        </ul>
    </li>

@endif
