@if(in_array('index_users', $permissions) || in_array('store_users', $permissions) || in_array('index_roles', $permissions) || in_array('store_roles', $permissions) || in_array('index_permissions', $permissions) || in_array('store_permissions', $permissions))

    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="fas fa-user-lock"></i>
            <p>
                {{__('nav.authorizations')}}
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>

        @if (in_array('index_users', $permissions) || in_array('store_users', $permissions))
            <ul class="nav nav-treeview ml-3 ">
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link">
                        <i class="fas fa-users"></i>
                        <p>
                            {{__('nav.users')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview  ml-3">
                        @if (in_array('store_users', $permissions))
                            <li class="nav-item">
                                <a href="{{action('UserController@create')}}" class="nav-link">
                                    <i class="far fa-plus-square"></i>
                                    <p>{{__('nav.create_users')}}</p>
                                </a>
                            </li>
                        @endif
                        @if (in_array('index_users', $permissions))
                            <li class="nav-item">
                                <a href="{{action('UserController@index')}}" class="nav-link">
                                    <i class="fas fa-list-ol"></i>
                                    <p>{{__('nav.index_users')}}</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        @endif


        @if (in_array('index_permissions', $permissions) || in_array('store_permissions', $permissions))
            <ul class="nav nav-treeview ml-3">
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link">
                        <i class="fas fa-chess-knight"></i>
                        <p>
                            {{__('nav.permissions')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview  ml-3">
                        @if (in_array('store_permissions', $permissions))
                            <li class="nav-item">
                                <a href="{{action('PermissionController@create')}}" class="nav-link">
                                    <i class="far fa-plus-square"></i>
                                    <p>{{__('nav.create_permissions')}}</p>
                                </a>
                            </li>
                        @endif
                        @if (in_array('index_permissions', $permissions))
                            <li class="nav-item">
                                <a href="{{action('PermissionController@index')}}" class="nav-link">
                                    <i class="fas fa-list-ol"></i>
                                    <p>{{__('nav.index_permissions')}}</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        @endif

        @if (in_array('index_roles', $permissions) || in_array('store_roles', $permissions))
            <ul class="nav nav-treeview ml-3">
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link">
                        <i class="fas fa-gamepad"></i>
                        <p>
                            {{__('nav.roles')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview  ml-3">
                        @if (in_array('store_roles', $permissions))
                            <li class="nav-item">
                                <a href="{{action('RoleController@create')}}" class="nav-link">
                                    <i class="far fa-plus-square"></i>
                                    <p>{{__('nav.create_roles')}}</p>
                                </a>
                            </li>
                        @endif
                        @if (in_array('index_roles', $permissions))
                            <li class="nav-item">
                                <a href="{{action('RoleController@index')}}" class="nav-link">
                                    <i class="fas fa-list-ol"></i>
                                    <p>{{__('nav.index_roles')}}</p>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            </ul>
        @endif
    </li>
@endif
