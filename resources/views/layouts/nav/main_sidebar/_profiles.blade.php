@if(in_array('index_profiles', $permissions) || in_array('store_profiles', $permissions) )

    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="far fa-id-badge"></i>
            <p>
                {{__('nav.profiles')}}
                <i class="right fas fa-angle-left"></i>
            </p>
        </a>
        <ul class="nav nav-treeview  ml-3">
            @if (in_array('store_profiles', $permissions))
                <li class="nav-item">
                    <a href="{{action('ProfileController@create')}}" class="nav-link">
                        <i class="far fa-plus-square"></i>
                        <p>{{__('nav.create_profiles')}}</p>
                    </a>
                </li>
            @endif
            @if (in_array('index_profiles', $permissions))
                <li class="nav-item">
                    <a href="{{action('ProfileController@index')}}" class="nav-link">
                        <i class="fas fa-list-ol"></i>
                        <p>{{__('nav.index_profiles')}}</p>
                    </a>
                </li>
            @endif
        </ul>
    </li>

@endif
