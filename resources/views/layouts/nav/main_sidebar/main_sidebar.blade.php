<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{asset('images/your_logo.png')}}" alt="your logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light h5">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if(auth()->check())
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img id="user-profile-image" src="{{asset('images/profiles/'. $personalImageName) }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info text-wrap">
                    <a href="{{$profileUrl}}" class="d-block">{{optional(auth()->user())->name}}</a>
                </div>
            </div>
        @endif

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                <!-- Authorization -->
                @include('layouts.nav.main_sidebar._authorization')
                <!-- Profiles -->
                @include('layouts.nav.main_sidebar._profiles')
                <!-- Hosts -->
                @include('layouts.nav.main_sidebar._hosts')
                <!-- Appointments -->
                @include('layouts.nav.main_sidebar._appointments')

                @if(auth()->check())
                    {!! Form::open(['action' => ['Auth\LoginController@logout'], 'files' => false, 'method'=>'POST'])!!}
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="this.closest('form').submit(); return false">
                                <i class="nav-icon text-warning fas fa-sign-out-alt"></i>
                                <p>{{__('nav.log_out')}}</p>
                            </a>
                        </li>
                    {!! Form::close() !!}
                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
