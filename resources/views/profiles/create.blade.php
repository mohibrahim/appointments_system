@extends('layouts.app')
@section('title')
	{{__('profile.add_profile')}}
@endsection
@section('content-name')
	{{__('profile.add_profile')}}
@endsection
@section('content')
	<div class="card card-info">
		<div class="card-body card-primary card-outline">
			@include('errors.list')
			{!! Form::open(['action' => ['ProfileController@store'], 'files'=>true, 'method'=>'POST'])!!}
				@include('profiles._form')
			{!! Form::close() !!}
		</div>
	</div>
@endsection
