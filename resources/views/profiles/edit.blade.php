@extends('layouts.app')
@section('title')
    {{__('profile.edit_profile')}}
@endsection
@section('content-name')
    {{__('profile.edit_profile')}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card card-primary card-outline">
			<div class="card-body">
				@include('errors.list')
				{!! Form::model($profile, ['action' => ['ProfileController@update', 'profile'=>$profile->id], 'files'=>true, 'method'=>'put'])!!}
					@include('profiles._form')
				{!! Form::close() !!}
			</div>
		</div>
    </div>
@endsection

