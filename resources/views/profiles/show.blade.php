@extends('layouts.app')

@section('title')
	{{__('profile.profile').' | '.optional($profile->user)->name}}
@endsection
@section('content-name')
    {{__('profile.profile')}}
@endsection
@section('content')



<div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
            <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{asset('images/profiles/'.$profile->personal_image)}}" alt="User profile picture">
            </div>
    
            <h3 class="profile-username text-center">{{optional($profile->user)->name}}</h3>
    
            <p class="text-muted text-center">Job Title</p>
    
            
            @if(in_array('update_profiles', $permissions))
                <a href="{{action('ProfileController@edit',['profile'=>$profile->id])}}" class="btn btn-primary btn-block">
                    <b><i class="far fa-edit"></i> {{__('profile.edit')}}</b>
                </a>
            @endif
            @if(in_array('destroy_profiles', $permissions))
                <button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('profile.delete')}}</button>
            @endif
        </div>
        <!-- /.card-body -->
    </div>

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="card card-primary card-outline">
        <div class="card-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th class="border-top-0">{{__('user.email')}}</th>
                        <td class="border-top-0" scop="row">
                        {{optional($profile->user)->email}}
                        </td>
                    </tr>

                    <tr>
                        <th class="border-top-0">{{__('profile.country')}}</th>
                        <td class="border-top-0" scop="row">
                        {{$profile->country}}
                        </td>
                    </tr>
                    <th class="border-top-0">{{__('profile.phone')}}</th>
                        <td class="border-top-0" scop="row">
                        {{$profile->phone}}
                        </td>
                    </tr>
                    <th class="border-top-0">{{__('profile.date_of_birth')}}</th>
                        <td class="border-top-0" scop="row">
                        {{optional($profile->date_of_birth)->format('Y-m-d')}}
                        </td>
                    </tr>
                    <tr>
                        <th class="border-top-0">Notes</th>
                        <td class="border-top-0" scope="row"> {{$profile->notes}} </td>
                    </tr>
                </tbody>
            </table>
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>

	@include('partial.deleteConfirm',['name'=>optional($profile->user)->name,
									  'message'=> __('profile.confirm_delete_message'),
									  'route'=> action('ProfileController@destroy', ['profile'=>$profile->id])])
@endsection
