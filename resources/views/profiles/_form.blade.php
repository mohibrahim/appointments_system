<div class="row">

	<div class="col-lg-12">
		<div class='form-group'>
			<span style='color:#f47373'>*</span>
			{!!Form::label('country', __('profile.country'))!!}
			{!!Form::select('country', $countries, null,['class'=>'form-control', 'placeholder'=>__('profile.country_placeholder')]) !!}
		</div>

		<div class='form-group'>
			<span style='color:#f47373'>*</span>
			{!!Form::label('phone', __('profile.phone'))!!}
			{!!Form::text('phone', null,['class'=>'form-control', 'placeholder'=>__('profile.phone_placeholder')]) !!}
		</div>

		<div class='form-group'>
			<span style='color:#f47373'>*</span>
			{!!Form::label('personal_image', __('profile.personal_image'))!!}
			{!!Form::file('personal_image', ['class'=>'form-control'])!!}

		</div>

		@if(!empty($profile->personal_image) && ($profile->personal_image != 'no_image.png'))
			<div id="personal-image-wrapper" class="form-group border rounded p-2">
				<img src="{{asset('images/profiles/'.$profile->personal_image)}}" alt="" width="50px">
				<button type="button" id="delete-personal-image" class="btn btn-danger btn-sm" data-personal-image-name="{{$profile->personal_image}}">Delete</button>
			</div>
		@endif

		<div class='form-group'>
			<span style='color:#f47373'>*</span>
			{!!Form::label('date_of_birth', __('profile.date_of_birth'))!!}
			{!!Form::date('date_of_birth', null,['class'=>'form-control', 'placeholder'=>__('profile.date_of_birth_placeholder')]) !!}
		</div>

		<div class='form-group'>
			{!!Form::label('notes', __('profile.notes'))!!}
			{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('profile.notes_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::submit(__('profile.save'), ['class'=>'btn btn-primary form-control'])!!}
		</div>
	</div>
</div>

@section('js_footer')
	<script>
		$(document).ready(function(){
			deletePersonalImage();
		});
	</script>
@endsection
