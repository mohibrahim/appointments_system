@extends('layouts.app')
@section('title')
	{{__('profile.all_profiles')}}
@endsection
@section('content-name')
    {{__('profile.all_profiles')}}
@endsection
@section('content')
		<div class="card">

				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('profile.searching_on_profile')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('profile.searching_word')}}" autofocus>
						</div>

						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('profile.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('profile.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('profile.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('profile.searching_button_search')}}">
					</div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover" id="profiles">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>User Name</th>
                                    <th>created at</th>
                                    <th>Creator name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($profiles as $profileKey => $profile)
                                    <tr>
                                        <td>{{$profileKey+1}}</td>
                                        <td>{{$profile->id}}</td>
                                        <td>
                                            {!!$profile->profile_show_link!!}
                                        </td>
                                        <td>{{$profile->created_at}}</td>
                                        <td>{{optional($profile->creator)->name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
				</div>
				<div class="d-flex justify-content-center">

					{{$profiles->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">


@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#profiles').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnProfilesAjax();
		});
	</script>
@endsection
