@extends('layouts.app')
@section('title')
Attendee Dashboard
@endsection
@section('content-name')
Dashoard
@endsection
@section('content')
<div class="card">
    <div class="card-body ">
        <div>
            <h2>Reserved Appointments</h2>
        </div>
        <br>
        <div class="row">
            @foreach($reservedAppointments as $reservedAppointment)
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="font-weight-bolder text-center"> {{$reservedAppointment->title}} </h3>
                            <p class="card-text">
                                <span class="font-weight-bold">Notes: </span>
                                {{$reservedAppointment->notes}}
                            </p>
                            <div class="card-text">
                                <span class="font-weight-bold"> Start:</span> {{$reservedAppointment->appointment_start}}
                            </div>
                            <div>
                                <span class="font-weight-bold">End:</span> {{$reservedAppointment->appointment_end}}
                            </div>
                            <div class="mb-3">
                                <div class="font-weight-bold">
                                    Host Name:
                                </div>
                                <h4>
                                    {{optional($reservedAppointment->host)->name}}
                                </h4>
                            </div>
                            <a href=" {{action('Dashboard\AttendeeDashboardController@joinAppointment', ['appointment_id'=>$reservedAppointment])}} " class="btn btn-primary">Join this meeting</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
</div>
<div class="card">
    <div class="card-body ">
        <div>
            <h2> Non-Reserved Appointments</h2>
        </div>
        <br>



        <div class="row">
            @foreach($nonReservedAppointments as $nonReservedAppointment)
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="font-weight-bolder text-center"> {{$nonReservedAppointment->title}} </h3>
                            <p class="card-text">
                                <span class="font-weight-bold">Notes: </span>
                                {{$nonReservedAppointment->notes}}
                            </p>
                            <div class="card-text">
                                <span class="font-weight-bold"> Start:</span> {{$nonReservedAppointment->appointment_start}}
                            </div>
                            <div>
                                <span class="font-weight-bold">End:</span> {{$nonReservedAppointment->appointment_end}}
                            </div>
                            <div class="mb-3">
                                <div class="font-weight-bold">
                                    Host Name:
                                </div>
                                <h4>
                                    {{optional($reservedAppointment->host)->name}}
                                </h4>
                            </div>
                            <a href="{{action('Dashboard\AttendeeDashboardController@reserveAppointment', ['appointment_id' => $nonReservedAppointment->id])}}" class="btn btn-primary">Reserve</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>



    </div>
</div>

@endsection
