@extends('layouts.app')
@section('title')
Attendee Rooom
@endsection
@section('head')
<!-- import #zmmtg-root css -->
<link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.6/css/bootstrap.css"/>
<link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.6/css/react-select.css"/>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Room</div>

                <div class="card-body">
                    <input type="hidden" id="re-call" data-insider="{{$appointmentId}}" data-type="attendee">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_footer')

    <script src="https://source.zoom.us/1.7.6/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/1.7.6/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/1.7.6/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/1.7.6/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/1.7.6/lib/vendor/jquery.min.js"></script>
    <script src="https://source.zoom.us/1.7.6/lib/vendor/lodash.min.js"></script>

    <script src="https://source.zoom.us/zoom-meeting-1.7.6.min.js"></script>
    <script src="{{asset('js/tool.js')}}"></script>
    <script src="{{asset('js/index.js')}}"></script>

@endsection
