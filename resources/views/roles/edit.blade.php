@extends('layouts.app')
@section('title')
	Edit Role
@endsection

@section('content-name')
    Edit Role
@endsection

@section('content')

    <div class="card">
        <div class="card-body ">
            <!-- validation errors -->
            @include('errors.list')
            <form class="" action="{{action('RoleController@update', ['role'=>$role->id])}}" method="post">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
                @include('roles._form')
            </form>
        </div>
    </div>

@endsection
