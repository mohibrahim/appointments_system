<div class="row">

	<div class="col-lg-12">

        <div class='form-group'>
            <span style='color:#f47373'>*</span>
            {!!Form::label('host_id', __('appointment.host_id'))!!}
            {!!Form::select('host_id', $appointment->hosts, null,['class'=>'form-control', 'placeholder'=>__('appointment.host_id_placeholder')]) !!}
        </div>

        <div class='form-group'>
            {!!Form::label('attendee_id', __('appointment.attendee_id'))!!}
            {!!Form::select('attendee_id', $appointment->attendees, null,['class'=>'form-control', 'placeholder'=>__('appointment.attendee_id_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('title', __('appointment.title'))!!}
			{!!Form::text('title', null,['class'=>'form-control', 'placeholder'=>__('appointment.title_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('appointment_start', __('appointment.appointment_start'))!!}
			{!!Form::dateTimeLocal('appointment_start', null,['class'=>'form-control', 'placeholder'=>__('appointment.appointment_start_placeholder')]) !!}
        </div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('appointment_end', __('appointment.appointment_end'))!!}
            {!!Form::dateTimeLocal('appointment_end', null,['class'=>'form-control', 'placeholder'=>__('appointment.appointment_end_placeholder')]) !!}
        </div>

		<div class='form-group'>
			{!!Form::label('notes', __('appointment.notes'))!!}
			{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('appointment.notes_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::submit(__('appointment.save'), ['class'=>'btn btn-primary form-control'])!!}
		</div>
	</div>
</div>
