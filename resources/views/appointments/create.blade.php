@extends('layouts.app')
@section('title')
	{{__('appointment.add_appointment')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{__('appointment.add_appointment')}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::open(['action' => ['AppointmentController@store'], 'files'=>true, 'method'=>'POST'])!!}
					@include('appointments._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
