@extends('layouts.app')

@section('title')
	{{$appointment->title}}
@endsection
@section('content-name')
    {{$appointment->title}}
@endsection
@section('content')
    <div class="card">
        <div class="card-body ">
            <table class="table">
                <tbody>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.id')}} </th>
                        <td class="border-top-0" scop="row">{{$appointment->id}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.title')}} </th>
                        <td class="border-top-0" scop="row"> {{$appointment->title}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.host_id')}} </th>
                        <td class="border-top-0" scop="row"> {{optional($appointment->host)->name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.attendee_id')}} </th>
                        <td class="border-top-0" scop="row"> {{optional($appointment->attendee)->name}}</td>
                    </tr>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.appointment_start')}} </th>
                        <td class="border-top-0" scope="row"> {{ $appointment->appointment_start}} </td>
                    </tr>
                    <tr>
                        <th class="border-top-0"> {{__('appointment.appointment_end')}} </th>
                        <td class="border-top-0" scope="row"> {{ $appointment->appointment_end}} </td>
                    </tr>

                    <tr>
                        <th class="border-top-0"> {{__('appointment.notes')}} </th>
                        <td class="border-top-0" scope="row"> {{$appointment->notes}} </td>
                    </tr>
                   <tr>
                        @if(in_array('update_appointments', $permissions))
                            <th class="border-top-0">Edit</th>
                            <td class="border-top-0" scope="row"> <a href="{{action('AppointmentController@edit',['appointment'=>$appointment->id])}}">Eidt</a> </td>
                        @endif
                    </tr>
                    <tr>
                        @if(in_array('destroy_appointments', $permissions))
                            <th class="border-top-0">Delete</th>
                            <td class="border-top-0" scope="row"><button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> Delete</button></td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


	@include('partial.deleteConfirm',['name'=>$appointment->title,
									  'message'=> __('appointment.confirm_delete_message'),
									  'route'=> action('AppointmentController@destroy', ['appointment'=>$appointment->id])])
@endsection
