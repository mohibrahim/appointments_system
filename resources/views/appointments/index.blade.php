@extends('layouts.app')
@section('title')
	{{__('appointment.all_appointments')}}
@endsection
@section('content-name')
    {{__('appointment.all_appointments')}}
@endsection
@section('content')
		<div class="card">

				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('appointment.searching_on_appointment')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('appointment.searching_word')}}" autofocus>
						</div>

						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('appointment.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('appointment.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('appointment.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('appointment.searching_button_search')}}">
					</div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover" id="appointments">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Created at</th>
                                    <th>Creator name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($appointments as $appointmentKey => $appointment)
                                    <tr>
                                        <td>{{$appointmentKey+1}}</td>
                                        <td>{{$appointment->id}}</td>
                                        <td>
                                            {!!$appointment->appointment_show_link!!}
                                        </td>

                                        <td>{{$appointment->appointment_start}}</td>
                                        <td>{{$appointment->appointment_end}}</td>
                                        <td>{{$appointment->created_at}}</td>
                                        <td>{{optional($appointment->creator)->name}}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
				</div>
				<div class="d-flex justify-content-center">
					{{$appointments->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">


@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#appointments').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnAppointmentsAjax();
		});
	</script>
@endsection
