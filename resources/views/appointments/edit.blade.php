@extends('layouts.app')
@section('title')
	Edit Appointment
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{$appointment->name}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::model($appointment, ['action' => ['AppointmentController@update', 'appointment'=>$appointment->id], 'files'=>true, 'method'=>'put'])!!}
					@include('appointments._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection

