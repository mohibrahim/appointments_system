@extends('layouts.app')
@section('title')
    All Permission
@endsection

@section('content-name')
    All Permissions
@endsection

@section('content')
    <div class="card">
        <div class="card-body ">
            <table class="table table-hover">
                <thead>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Name</th>
                </thead>
                <tbody>
                    @foreach($permissions as $permission)
                        <tr>
                            <td>{{$permission->id}}</td>
                            <td>{{$permission->title}}</td>
                            <td>
                                <a href="{{ action('PermissionController@show',['permission' => $permission->id]) }}" >
                                    {{$permission->name}}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
