@extends('layouts.app')
@section('title')
	Edit Permission
@endsection

@section('content-name')
    Update Permission
@endsection

@section('content')
    <div class="card ">
        <div class="card-body ">
            @include('errors.list')
            <form class="form" action="{{action('PermissionController@update', ['permission'=>$permission->id])}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                @include('permissions._form')
            </form>
        </div>
    </div>
@endsection
