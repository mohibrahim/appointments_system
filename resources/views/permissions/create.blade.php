@extends('layouts/app')
@section('title')
	Create Permission
@endsection

@section('content-name')
    Add New Permission
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @include('errors.list')
            <form class="" action="{{ action('PermissionController@store') }}" method="post">
                {{ csrf_field() }}
                @include('permissions._form')
            </form>
        </div>
    </div>
@endsection()
