<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
|--------------------------------------------------------------------------
| Hosts
|--------------------------------------------------------------------------
|
*/
Route::resource('hosts', 'HostController');
Route::post('hosts_miscellaneous/index_search', 'HostController@search')->name('hosts_miscellaneous.index_search');
/*
|--------------------------------------------------------------------------
| Appointments
|--------------------------------------------------------------------------
|
*/
Route::resource('appointments', 'AppointmentController');
Route::post('appointments_miscellaneous/index_search', 'AppointmentController@search')->name('appointments_miscellaneous.index_search');

/*
|--------------------------------------------------------------------------
| Admin Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get('admin_dashboard/home', 'Dashboard\AdminDashboardController@home')
->name('admin_dashboard.home');
/*
|--------------------------------------------------------------------------
| Host Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get('host_dashboard/home', 'Dashboard\HostDashboardController@home')
->name('host_dashboard.home');
Route::get('host_dashboard/join_appointment', 'Dashboard\HostDashboardController@joinAppointment')
    ->name('host_dashboard.join_appointment');
Route::post('host_dashboard/join_appointment_get_signature', 'Dashboard\HostDashboardController@joinAppointmentGetSignature')
    ->name('host_dashboard.join_appointment_get_signature');
/*
|--------------------------------------------------------------------------
| Attendee Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get('attendee_dashboard/home', 'Dashboard\AttendeeDashboardController@home')
    ->name('attendee_dashboard.home');
Route::get('attendee_dashboard/reserve_appointment', 'Dashboard\AttendeeDashboardController@reserveAppointment')
    ->name('attendee_dashboard.reserve_appointment');
Route::get('attendee_dashboard/join_appointment', 'Dashboard\AttendeeDashboardController@joinAppointment')
    ->name('attendee_dashboard.join_appointment');
Route::post('attendee_dashboard/join_appointment_get_signature', 'Dashboard\AttendeeDashboardController@joinAppointmentGetSignature')
    ->name('attendee_dashboard.join_appointment_get_signature');
