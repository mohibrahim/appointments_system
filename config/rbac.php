<?php

return [
    'available_languages' => ['ar', 'en'],

    //[role_name=>dashboard_class_name]
    'roles_names_dashboard_controllers_names' => [
        'Developer' => 'Developer',
        'Admin' => 'Admin',
        'Host' => 'Host',
        'Attendee' => 'Attendee',
    ]
];
