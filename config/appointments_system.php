<?php



return [
    'zoom_integration' => [
        'api_key' => env('ZOOM_API_KEY', 'null'),
        'secret_key' => env('ZOOM_SECRET_KEY', 'null'),
        'jwt_email' => env('ZOOM_JWT_EMAIL', 'null'),
        'jwt_token' => env('ZOOM_JWT_TOKEN', 'null'),
    ]
];
