<?php
namespace App\Services\Currencies\WorldCurrenciesStrategy;

use App\Services\LanguageLocator\LanguageLocatorTrait;



class SuitableWorldCurrencies
{
    use LanguageLocatorTrait;

    private $WorldCurrencyArabic;
    private $WorldCurrencyEnglish;


    public function __construct(WorldCurrencyArabic $WorldCurrencyArabic, WorldCurrencyEnglish $WorldCurrencyEnglish)
    {
        $this->WorldCurrencyArabic = $WorldCurrencyArabic;
        $this->WorldCurrencyEnglish = $WorldCurrencyEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->WorldCurrencyArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->WorldCurrencyEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->WorldCurrencyArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->WorldCurrencyEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        }
        return $value;
    }
}
