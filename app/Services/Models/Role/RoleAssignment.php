<?php

namespace App\Services\Models\Role;

use App\Role;
use App\User;
use App\Services\Models\User\Type\UserType;

trait RoleAssignment
{
    public function assignRule(User $user): User
    {
        if ($user->type == UserType::ADMIN) {
            $adminRole = Role::where('name', UserType::ADMIN)
                ->firstOrFail();
            $user->roles()->attach($adminRole);

            $user->update(['master_role_id' => $adminRole->id]);

            return $user;
        }
        if ($user->type == UserType::HOST) {
            $hostRole = Role::where('name', UserType::HOST)
            ->firstOrFail();
            $user->roles()->attach($hostRole);

            $user->update(['master_role_id' => $hostRole->id]);

            return $user;
        }
        if ($user->type == UserType::ATTENDEE) {
            $attendeeRole = Role::where('name', UserType::ATTENDEE)
            ->firstOrFail();
            $user->roles()->attach($attendeeRole);

            $user->update(['master_role_id' => $attendeeRole->id]);

            return $user;
        }
        return $user;
    }
}
