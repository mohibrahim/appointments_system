<?php

namespace App\Services\Models\Appointment\DeleteHostStrategy;

use App\Appointment;

interface DeleteAppointmentInterface
{
    public function delete(Appointment $host): bool;
}
