<?php

namespace App\Services\Models\Appointment\DeleteHostStrategy;

use App\Appointment;

class DeleteAppointment implements DeleteAppointmentInterface
{
    public function delete(Appointment $host): bool
    {
        $host->delete();
        return true;

    }
}
