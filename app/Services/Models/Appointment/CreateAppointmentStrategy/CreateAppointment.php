<?php

namespace App\Services\Models\Appointment\CreateAppointmentStrategy;

use App\Appointment;
use Illuminate\Http\Request;
use App\Services\CloudMeeting\Push\CreateMeetingStrategy\CreateMeetingInterface;

class CreateAppointment implements CreateAppointmentInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Appointment
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $appointment = Appointment::create($attributes);
        $meetingId = $this->createZoomMeeting($appointment);
        $appointment->update(['zoom_meeting_number' => $meetingId]);
        return $appointment;
    }

    private function createZoomMeeting(Appointment $appointment): string
    {
        $url = 'https://api.zoom.us/v2/users/'.config('appointments_system.zoom_integration.jwt_email').'/meetings';
        $authorization = config('appointments_system.zoom_integration.jwt_token');

        $startTime = $appointment->appointment_start;
        $endTime = $appointment->appointment_end;
        $duration = $startTime->diffInMinutes($endTime);
        $topic = $appointment->title;

        $data = [
            "duration"=> $duration,
            "start_time"=> $startTime,
            "status"=> "waiting",
            "timezone"=> "Africa/Cairo",
            "topic"=> $topic,
            "type"=> 2,
            "password"=>"123321"
        ];

        $meetingCreator = resolve(CreateMeetingInterface::class);
        $meetingId = $meetingCreator->send($url, $authorization, $data);
        return $meetingId;
    }

}
