<?php

namespace App\Services\Models\Appointment\CreateAppointmentStrategy;

use App\Appointment;
use Illuminate\Http\Request;


interface CreateAppointmentInterface
{
    public function create(Request $request): Appointment;
}
