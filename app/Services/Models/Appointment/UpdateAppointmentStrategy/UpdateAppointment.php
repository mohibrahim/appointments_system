<?php

namespace App\Services\Models\Appointment\UpdateAppointmentStrategy;

use App\Appointment;
use Illuminate\Http\Request;

class UpdateAppointment implements UpdateAppointmentInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($appointmentId, Request $request): Appointment
    {
        $host = Appointment::findOrFail($appointmentId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $host->update($attributes);
        return $host;
    }
}
