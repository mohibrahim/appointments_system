<?php

namespace App\Services\Models\Appointment\UpdateAppointmentStrategy;

use App\Appointment;
use Illuminate\Http\Request;


interface UpdateAppointmentInterface
{
    public function update($appointmentId, Request $request): Appointment;
}
