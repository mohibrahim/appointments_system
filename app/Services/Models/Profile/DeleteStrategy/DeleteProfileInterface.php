<?php

namespace App\Services\Models\Profile\DeleteStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploaderInterface;

interface DeleteProfileInterface
{
    public function delete(Profile $profile, FileUploaderInterface $fileUploaderInterface): bool;
}