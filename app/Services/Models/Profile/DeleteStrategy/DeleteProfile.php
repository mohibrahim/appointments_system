<?php

namespace App\Services\Models\Profile\DeleteStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploaderInterface;

class DeleteProfile implements DeleteProfileInterface
{
    public function delete(Profile $profile, FileUploaderInterface $fileUploader): bool
    {
        $fileUploader->delete($profile->personal_image, 'images/profiles', 'no_image.png');
        $profile->delete();
        return true;
        
    }
}