<?php

namespace App\Services\Models\Profile\CreateProfileStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploaderInterface;
use Illuminate\Http\Request;


interface CreateProfileInterface
{
    public function create(Request $request, FileUploaderInterface $fileUploaderInterface): Profile;
}
