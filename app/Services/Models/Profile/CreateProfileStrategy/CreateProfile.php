<?php

namespace App\Services\Models\Profile\CreateProfileStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploaderInterface;
use Illuminate\Http\Request;

class CreateProfile implements CreateProfileInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request, FileUploaderInterface $fileUploader): Profile
    {
        $attributes = $fileUploader->add($request, 'personal_image', 'images/profiles');
        
        $attributes['creator_id'] = auth()->id();
        $profile = Profile::create($attributes);
        return $profile;
    }

}
