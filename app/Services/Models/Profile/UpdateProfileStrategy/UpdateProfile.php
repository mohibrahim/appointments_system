<?php

namespace App\Services\Models\Profile\UpdateProfileStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\FileUploader\FileUploaderInterface;

class UpdateProfile implements UpdateProfileInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($profileId, Request $request, FileUploaderInterface $fileUploader): Profile
    {
        $profile = Profile::findOrFail($profileId);

        $fileUploader = resolve(FileUploaderInterface::class);
        $fileUploader->setFileNameToSkip('no_image.png');
        $attributes = $fileUploader->update($request, 'personal_image', $profile->personal_image, 'images/profiles');

        $attributes['last_updater_id'] = auth()->id();

        $profile->update($attributes);
        return $profile;
    }

}
