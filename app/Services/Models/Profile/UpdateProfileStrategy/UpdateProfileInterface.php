<?php

namespace App\Services\Models\Profile\UpdateProfileStrategy;

use App\Profile;
use App\Services\FileUploader\FileUploaderInterface;
use Illuminate\Http\Request;


interface UpdateProfileInterface
{
    public function update($userId, Request $request, FileUploaderInterface $fileUploaderInterface): Profile;
}
