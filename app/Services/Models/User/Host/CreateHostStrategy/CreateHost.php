<?php

namespace App\Services\Models\User\Host\CreateHostStrategy;

use App\Services\Models\Role\RoleAssignment;
use App\Services\Models\User\Host\HostTrait;
use App\Services\Models\User\Type\UserType;
use App\User;
use Illuminate\Http\Request;

class CreateHost implements CreateHostInterface
{
    use HostTrait;
    use RoleAssignment;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): User
    {
        $authenticatedUserId = auth()->id();

        $attributes = $request->all();

        $attributes['type'] = UserType::HOST;
        $attributes['creator_id'] = $authenticatedUserId;
        $attributes['admin_who_generate_hosts_id'] = $authenticatedUserId;

        $attributes = $this->hashPassword($attributes);

        $host = User::create($attributes);
        $host = $this->assignRule($host);

        return $host;
    }

}
