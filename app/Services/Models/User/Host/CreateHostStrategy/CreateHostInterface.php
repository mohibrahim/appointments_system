<?php

namespace App\Services\Models\User\Host\CreateHostStrategy;

use App\User;
use Illuminate\Http\Request;


interface CreateHostInterface
{
    public function create(Request $request): User;
}
