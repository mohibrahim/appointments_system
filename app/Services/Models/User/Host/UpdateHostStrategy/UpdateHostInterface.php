<?php

namespace App\Services\Models\User\Host\UpdateHostStrategy;

use App\User;
use Illuminate\Http\Request;


interface UpdateHostInterface
{
    public function update($hostId, Request $request): User;
}
