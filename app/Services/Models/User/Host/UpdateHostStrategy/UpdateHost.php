<?php

namespace App\Services\Models\User\Host\UpdateHostStrategy;

use App\User;
use Illuminate\Http\Request;
use App\Services\Models\User\Type\UserType;
use App\Services\Models\User\Host\HostTrait;

class UpdateHost implements UpdateHostInterface
{
    use HostTrait;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($hostId, Request $request): User
    {
        $host = User::where('id', $hostId)
            ->where('type', UserType::HOST)
            ->firstOrFail();
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $attributes = $this->hashPassword($attributes);
        $host->update($attributes);
        return $host;
    }
}
