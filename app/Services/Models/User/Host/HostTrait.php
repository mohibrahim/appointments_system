<?php

namespace App\Services\Models\User\Host;

use Illuminate\Support\Facades\Hash;

trait HostTrait
{

    protected function hashPassword(array $attributes): array
    {
        if (!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
            unset($attributes['password_confirmation']);
        }

        if (empty($attributes['password'])) {
            unset($attributes['password']);
            unset($attributes['password_confirmation']);
        }
        return $attributes;
    }
}
