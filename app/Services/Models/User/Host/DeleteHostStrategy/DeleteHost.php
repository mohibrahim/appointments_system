<?php

namespace App\Services\Models\User\Host\DeleteHostStrategy;

use App\User;

class DeleteHost implements DeleteHostInterface
{
    public function delete(User $host): bool
    {
        $host->delete();
        return true;

    }
}
