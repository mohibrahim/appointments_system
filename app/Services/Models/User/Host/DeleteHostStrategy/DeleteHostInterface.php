<?php

namespace App\Services\Models\User\Host\DeleteHostStrategy;

use App\User;

interface DeleteHostInterface
{
    public function delete(User $host): bool;
}
