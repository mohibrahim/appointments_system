<?php

namespace App\Services\Models\User\CreateUserStrategy;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\Models\Role\RoleAssignment;

class CreateUser implements CreateUserInterface
{
    use RoleAssignment;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): User
    {
        $attributes = $request->all();
        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['creator_id'] = auth()->id();
        $user = User::create($attributes);
        $user = $this->assignRule($user);
        //creating user profile
        $user->profile()->create(['creator_id'=>$user->id]);
        return $user;
    }




}
