<?php

namespace App\Services\Models\User\Type;


class UserType extends UserTypeAbstract
{

    public static function toArray(): array
    {
        return [
            self::ADMIN => __('user.type_admin'),
            self::HOST => __('user.type_host'),
            self::ATTENDEE => __('user.type_attendee'),
            self::OTHER => __('user.type_other'),
        ];
    }

    public static function selectedValue(?string $key): string
    {
        $value = '';
        $list = self::toArray();
        if (empty($list[$key])) {
            return $value;
        }
        $value = $list[$key];
        return $value;
    }
}
