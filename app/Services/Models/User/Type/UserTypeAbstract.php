<?php

namespace App\Services\Models\User\Type;


abstract class UserTypeAbstract
{
    public const ADMIN = 'admin';
    public const HOST = 'host';
    public const ATTENDEE = 'attendee';
    public const OTHER = 'other';

    public static abstract function toArray(): array;
    public static abstract function selectedValue(?string $key): string;
}
