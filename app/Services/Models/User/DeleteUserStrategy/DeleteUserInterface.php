<?php

namespace App\Services\Models\User\DeleteUserStrategy;

use App\Services\FileUploader\FileUploaderInterface;
use App\User;

interface DeleteUserInterface
{
    public function delete(User $user, FileUploaderInterface $fileUploaderInterface): bool;
}