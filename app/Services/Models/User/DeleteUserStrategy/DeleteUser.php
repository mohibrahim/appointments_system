<?php

namespace App\Services\Models\User\DeleteUserStrategy;

use App\Services\FileUploader\FileUploaderInterface;
use App\User;

class DeleteUser implements DeleteUserInterface
{
    public function delete(User $user, FileUploaderInterface $fileUploaderInterface): bool
    {
        //Remove profile image
        $profile = $user->profile;
        if (!empty($profile)) {
            $fileUploaderInterface->delete($profile->personal_image, 'images/profiles', 'no_image.png');
        }
        $user->delete();
        return true;
    }
}