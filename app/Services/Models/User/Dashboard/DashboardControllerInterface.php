<?php

namespace App\Services\Models\User\Dashboard;

interface DashboardControllerInterface
{
    /**
     * Undocumented function
     *
     * @return Illuminate\Http\Response
     */
    public function home();
}
