<?php

namespace App\Services\Models\User\Dashboard;


class DashboardSpecifier
{
    public function specifyUserDashboard()
    {
        $masterRoleName = $this->getAuthenticatedUserMasterRoleName();
        return $this->chooseDashboard($masterRoleName);
    }

    private function getAuthenticatedUserMasterRoleName(): string
    {
        $authenticatedUser = auth()->user();
        if (empty($authenticatedUser)) {
            return '';
        }

        $masterRole = $authenticatedUser->masterRole;
        if (empty($authenticatedUser->masterRole)) {
            return '';
        }

        return ltrim(ucwords($masterRole->name), ' ');
    }

    private function chooseDashboard(string $masterRoleName)
    {
        $rolesNamesDashboardControllersNames = config('rbac.roles_names_dashboard_controllers_names');
        if (array_key_exists($masterRoleName, $rolesNamesDashboardControllersNames)) {
            return redirect()->action(
                'Dashboard\\' . $rolesNamesDashboardControllersNames[$masterRoleName] . 'DashboardController@home'
            );
        }
        return view('home');
    }

    public function isAllowedForDashboard(string $dashboardName): bool
    {
        $masterRoleName = $this->getAuthenticatedUserMasterRoleName();
        if ($dashboardName == $masterRoleName) {
            return true;
        }
        return false;
    }
}
