<?php

namespace App\Services\Models\User\UpdateUserStrategy;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdateUser implements UpdateUserInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($userId, Request $request): User
    {
        $user = User::findOrFail($userId);
        $attributes = $request->all();
        if (!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        } else {
            unset($attributes['password']);
        }


        $isUpdated = $user->update($attributes);
        return $user;
    }

}
