<?php

namespace App\Services\CloudMeeting\Push\CreateMeetingStrategy;


interface CreateMeetingInterface
{
    public function send(string $url, string $authorization, array $data): string;
}
