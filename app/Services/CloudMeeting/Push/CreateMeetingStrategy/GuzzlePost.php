<?php

namespace App\Services\CloudMeeting\Push\CreateMeetingStrategy;

use GuzzleHttp\Client;


class GuzzlePost implements CreateMeetingInterface
{
    public function send(string $url, string $authorization, array $data): string
    {
        $client = new Client();
        $response = $client->post(
            $url,
            [
                'headers' => [
                    'authorization' => $authorization,
                    'Content-type' => 'application/json'
                ],
                'json' =>  $data,
            ]
        );
        $stringResponseAsJson = $response->getBody()->getContents();
        $responseAsObject = json_decode($stringResponseAsJson);

        return $responseAsObject->id;
    }
}
