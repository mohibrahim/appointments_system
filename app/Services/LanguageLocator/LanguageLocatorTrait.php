<?php

namespace App\Services\LanguageLocator;

use Illuminate\Support\Facades\Cookie;

/**
 *
 */
trait LanguageLocatorTrait
{
    public function isArabic()
	{
		if (Cookie::has('current_language') && Cookie::get('current_language') == 'ar'){
			return true;
		} elseif (app()->getLocale() == 'ar') {
			return true;
		}
		return false;
	}

	public function isEnglish()
	{
		if (Cookie::has('current_language') && Cookie::get('current_language') == 'en') {
			return true;
		} elseif (app()->getLocale() == 'en') {
			return true;
		}
		return false;
	}
}
