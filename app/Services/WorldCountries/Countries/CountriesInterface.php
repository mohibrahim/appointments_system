<?php

namespace App\Services\WorldCountries\Countries;

interface CountriesInterface
{
    public function getCountries(): array;
}
