<?php

namespace App\Services\WorldCountries;


interface LanguageCountriesResolverInterface
{
    public function getSuitableCountriesListByCurrentLanguage(): array;
    public function getSuitableCountryByCurrentLanguage(string $countryKey): string;
}
