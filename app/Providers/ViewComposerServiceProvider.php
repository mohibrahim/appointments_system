<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeNavigationPrivileges();
    }

    private function composeNavigationPrivileges()
    {
        $v = [
            'layouts.app',
            'permissions.show',

            'roles.show',
            'roles.create',
            'roles.edit',


            'users.show',
            'users._form',

            'role_user._form',

            'profiles.show',
            'profiles._form',

            'users.hosts.create',
            'users.hosts.show',

            'appointments.show',
        ];
        view()->composer($v, function ($view) {

            $permissions = [];

            $aUser = Auth::user();
            if (isset($aUser)) {
                $userRoles = $aUser->roles;
                if ($userRoles->isNotEmpty()) {
                    foreach ($userRoles as $role) {
                        $permissions = array_merge(
                            $permissions,
                            $role->permissions()->pluck('name')->toArray()
                        );
                    }
                }
            }

            $data = [];
            $data['permissions'] = $permissions;

            $profileData = $this->profileImageNameAndShowLink(auth()->user());
            $data = array_merge($data, $profileData);
            $view->with($data);
        });
    }

    private function profileImageNameAndShowLink($authenticatedUser): array
    {
        $data = [
            'personalImageName' => 'no_image.png',
            'profileUrl' => '#'
        ];

        if (empty($authenticatedUser)) {
            return $data;
        }

        $profile = $authenticatedUser->profile;
        if (empty($profile)) {
            return $data;
        }

        $profileImage = $profile->personal_image;
        if (!empty($profileImage)) {
            $data['personalImageName'] = $profileImage;
        }

        $profileId = $profile->id;
        if (!empty($profileId)) {
            $data['profileUrl'] = action('ProfileController@show', ['profile'=>$profileId]);
        }
        return $data;
    }
}
