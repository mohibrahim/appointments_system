<?php
namespace App\Providers;

use App\Services\CloudMeeting\Push\CreateMeetingStrategy\CreateMeetingInterface;
use App\Services\CloudMeeting\Push\CreateMeetingStrategy\GuzzlePost;
use Illuminate\Support\ServiceProvider;
use App\Services\FileUploader\FileUploader;
use App\Services\IndexTenancy\IndexByTenancy;
use App\Services\FileUploader\FileUploaderInterface;
use App\Services\IndexTenancy\IndexStrategyInterface;
use App\Services\Models\User\CreateUserStrategy\CreateUser;
use App\Services\Models\User\DeleteUserStrategy\DeleteUser;
use App\Services\Models\User\UpdateUserStrategy\UpdateUser;
use App\Services\Models\Profile\DeleteStrategy\DeleteProfile;
use App\Services\WorldCountries\ArabicEnglishCountriesResolver;
use App\Services\Models\User\Host\CreateHostStrategy\CreateHost;
use App\Services\Models\User\Host\DeleteHostStrategy\DeleteHost;
use App\Services\Models\User\Host\UpdateHostStrategy\UpdateHost;
use App\Services\WorldCountries\LanguageCountriesResolverInterface;
use App\Services\Models\Profile\CreateProfileStrategy\CreateProfile;
use App\Services\Models\Profile\UpdateProfileStrategy\UpdateProfile;
use App\Services\Models\User\CreateUserStrategy\CreateUserInterface;
use App\Services\Models\User\DeleteUserStrategy\DeleteUserInterface;
use App\Services\Models\User\UpdateUserStrategy\UpdateUserInterface;
use App\Services\Models\Profile\DeleteStrategy\DeleteProfileInterface;
use App\Services\Models\Appointment\DeleteHostStrategy\DeleteAppointment;
use App\Services\Models\User\Host\CreateHostStrategy\CreateHostInterface;
use App\Services\Models\User\Host\DeleteHostStrategy\DeleteHostInterface;
use App\Services\Models\User\Host\UpdateHostStrategy\UpdateHostInterface;
use App\Services\Models\Profile\CreateProfileStrategy\CreateProfileInterface;
use App\Services\Models\Profile\UpdateProfileStrategy\UpdateProfileInterface;
use App\Services\Models\Appointment\CreateAppointmentStrategy\CreateAppointment;
use App\Services\Models\Appointment\UpdateAppointmentStrategy\UpdateAppointment;
use App\Services\Models\Appointment\DeleteHostStrategy\DeleteAppointmentInterface;
use App\Services\Models\Appointment\CreateAppointmentStrategy\CreateAppointmentInterface;
use App\Services\Models\Appointment\UpdateAppointmentStrategy\UpdateAppointmentInterface;

class InterfaceResolverServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(IndexStrategyInterface::class, IndexByTenancy::class);
        $this->app->bind(LanguageCountriesResolverInterface::class, ArabicEnglishCountriesResolver::class);
        $this->app->bind(FileUploaderInterface::class, FileUploader::class );
        $this->app->bind(CreateUserInterface::class, CreateUser::class);
        $this->app->bind(UpdateUserInterface::class, UpdateUser::class );
        $this->app->bind(DeleteUserInterface::class, DeleteUser::class );
        $this->app->bind(CreateProfileInterface::class, CreateProfile::class);
        $this->app->bind(UpdateProfileInterface::class, UpdateProfile::class );
        $this->app->bind(DeleteProfileInterface::class, DeleteProfile::class );

        $this->app->bind(CreateHostInterface::class, CreateHost::class );
        $this->app->bind(DeleteHostInterface::class, DeleteHost::class );
        $this->app->bind(UpdateHostInterface::class, UpdateHost::class );

        $this->app->bind(CreateAppointmentInterface::class, CreateAppointment::class );
        $this->app->bind(DeleteAppointmentInterface::class, DeleteAppointment::class );
        $this->app->bind(UpdateAppointmentInterface::class, UpdateAppointment::class );

        $this->app->bind(CreateMeetingInterface::class, GuzzlePost::class);
    }
}
