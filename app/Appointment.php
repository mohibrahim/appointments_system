<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Services\Models\User\Type\UserType;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Appointment extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    public $table = 'appointments';
    public $fillable = [
        'title',
        'appointment_start',
        'appointment_end',
        'notes',
        'creator_id',
        'last_updater_id',
        'host_id',
        'attendee_id',
        'zoom_meeting_number',
    ];

    public $dates = ['appointment_start', 'appointment_end'];
    public $appends = ['appointment_show_link'];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getAppointmentShowLinkAttribute()
    {
        return '<a href="' . action('AppointmentController@show', ['appointment' => $this->id]) . '" target="_blank">' . $this->title . '</a>';
    }

    public function getHostsAttribute()
    {
        return User::where('type', UserType::HOST)
            ->get()
            ->pluck('name', 'id');
    }

    public function getAttendeesAttribute()
    {
        return User::where('type', UserType::ATTENDEE)
            ->get()
            ->pluck('name', 'id');
    }

    public function getAppointmentStartAttribute($date)
    {
        if (empty($date)) {
            return '';
        }
        if (!empty($date) && request()->routeIs('appointments.edit')) {
            return $this->asDateTime($date)->format('Y-m-d\TH:i');
        }
        if (!empty($date) && request()->routeIs('appointments.store')) {
            return $this->asDateTime($date);
        }
        return $this->asDateTime($date)->format('Y-m-d h:m A');
    }

    public function getAppointmentEndAttribute($date)
    {
        if (empty($date)) {
            return '';
        }
        if (!empty($date) && request()->routeIs('appointments.edit')) {
            return $this->asDateTime($date)->format('Y-m-d\TH:i');
        }
        if (!empty($date) && request()->routeIs('appointments.store')) {
            return $this->asDateTime($date);
        }
        return $this->asDateTime($date)->format('Y-m-d h:m A');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the user who create this appointment.
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last user who updated this appointment.
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the host that are associated with this appointment.
     *
     * @return BelongsTo
     */
    public function host(): BelongsTo
    {
        return $this->belongsTo(User::class, 'host_id', 'id');
    }


    /**
     * Getting the attendee user that are associated with this appointment
     *
     * @return BelongsTo
     */
    public function attendee(): BelongsTo
    {
        return $this->belongsTo(User::class, 'attendee_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, $keyword)
    {
        $results = $builder->with('creator')
            ->where('title', 'like', "%$keyword%")
            ->get();
        return $results;
    }
}
