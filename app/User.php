<?php

namespace App;

use App\Role;
use App\Services\Models\User\Type\UserType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable
{
    use Notifiable;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'first_name', 'last_name', 'email', 'password', 'notes', 'creator_id', 'last_updater_id',
        'master_role_id', 'admin_who_generate_hosts_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'name',
        'user_show_link',
        'host_show_link',
    ];

    /*
    |--------------------------------------------------------------------------
    | Getters and Setters
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getUserShowLinkAttribute(): string
    {
        return '<a href="' . action('UserController@show', ['user' => $this->id]) . '" target="_blank">' . $this->name . '</a>';
    }

    public function getUserRolesNamesIdsAttribute(): array
    {
        $roles = $this->roles;
        if ($roles->isEmpty()) {
            return [];
        }
        return $roles->pluck('name', 'id')->toArray();
    }

    public function getHostShowLinkAttribute()
    {
        return '<a href="' . action('HostController@show', ['host' => $this->id]) . '" target="_blank">' . $this->name . '</a>';
    }

    public function getTypesAttribute()
    {
        return UserType::toArray();
    }

    public function getTypeAttribute($type)
    {
        $routeName = request()->route()
            ->getName();

        if (
            $routeName == 'users.show' ||
            $routeName == 'users.index' ||
            $routeName == 'users_miscellaneous.index_search'
        ) {
            return UserType::selectedValue($type);
        }
        return $type;
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the users that are created by creator user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return HasMany
     */
    public function createdUsers(): HasMany
    {
        return $this->hasMany(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the users that are updated by the last updater user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return HasMany
     */
    public function updatedUsers(): HasMany
    {
        return $this->hasMany(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the permissions that are created by this user.
     *
     * @return HasMany
     */
    public function createdPermissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'creator_id', 'id');
    }

    /**
     * Getting the roles that are created by this user.
     *
     * @return HasMany
     */
    public function createdRoles(): HasMany
    {
        return $this->hasMany(Role::class, 'creator_id', 'id');
    }

    /**
     * Getting the profiles that are created by this user.
     *
     * @return HasMany
     */
    public function createdProfiles(): HasMany
    {
        return $this->hasMany(Profile::class, 'creator_id', 'id');
    }

    /**
     * Getting the profiles that are created by this user.
     *
     * @return HasMany
     */
    public function createdAppointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'creator_id', 'id');
    }

    //*************************** */

    /**
     * Getting the permissions that are created by this user.
     *
     * @return HasMany
     */
    public function updatedPermissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the roles that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedRoles(): HasMany
    {
        return $this->hasMany(Role::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the profiles that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedProfiles(): HasMany
    {
        return $this->hasMany(Profile::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the profiles that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedAppointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'last_updater_id', 'id');
    }



    //-------------------------


    public function masterRole(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'master_role_id', 'id');
    }

    /**
     * Getting the roles that are associated with this user.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    /**
     * Getting user profile
     *
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }

    /**
     * Admin perspective
     * Getting hosts that are associated with this admin
     * relationship type: Recursive
     * @return HasMany
     */
    public function hostsGeneratedByAdmin(): HasMany
    {
        return $this->hasMany(User::class, 'admin_who_generate_hosts_id', 'id');
    }

    /**
     * Host perspective
     * Getting the admin that generate this host
     * relationship type: Recursive
     * @return BelongsTo
     */
    public function adminWhoGenerateHost(): BelongsTo
    {
        return $this->belongsTo(User::class, 'admin_who_generate_hosts_id', 'id');
    }

    /**
     * Getting the appointments that are assigned for this host.
     *
     * @return HasMany
     */
    public function hostAppointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'host_id', 'id');
    }

    /**
     * Getting the appointments reserved by this attendee.
     *
     * @return HasMany
     */
    public function attendeeAppointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'attendee_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
     */

    public function getPermissionsNamesAsArray(): array
    {
        $roles = $this->roles;
        $permissionsNamesAsArray = [];
        if ($roles->isNotEmpty()) {
            foreach ($roles as $role) {
                $permissions = $role->permissions;
                if ($permissions->isNotEmpty()) {
                    foreach ($permissions as $permission) {
                        $permissionsNamesAsArray[] = $permission->name;
                    }
                }
            }
        }
        return $permissionsNamesAsArray;
    }

    public function search(Builder $builder, $keyword)
    {
        $name = explode(' ', $keyword);
        $firstName = isset($name[0])?($name[0]):('');
        $lastName = (count($name) > 1)?($name[1]):('');

        if ($firstName && !$lastName) {
            $results = $builder->with('roles', 'creator')
                ->where('first_name', 'like', "%$firstName%")
                ->get();
            return $results;
        }

        if ($firstName && $lastName) {
            $results = $builder->with('roles', 'creator')
                ->where('first_name', 'like', "%$firstName%")
                ->Where('last_name', 'like', "%$lastName%")
                ->get();
            return $results;
        }
    }

    public function searchForHost(Builder $builder, $keyword)
    {
        $name = explode(' ', $keyword);
        $firstName = isset($name[0])?($name[0]):('');
        $lastName = (count($name) > 1)?($name[1]):('');

        $builder = $builder->where('type', UserType::HOST);

        if ($firstName && !$lastName) {
            $results = $builder->with('roles', 'creator')
                ->where('first_name', 'like', "%$firstName%")
                ->get();
            return $results;
        }

        if ($firstName && $lastName) {
            $results = $builder->with('roles', 'creator')
                ->where('first_name', 'like', "%$firstName%")
                ->Where('last_name', 'like', "%$lastName%")
                ->get();
            return $results;
        }
    }
}
