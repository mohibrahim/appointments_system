<?php

namespace App\Http\Middleware;

use Closure;

class RoleUserRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];
        
        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'role_user.edit' && in_array('update_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'role_user.update' && in_array('update_users', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
