<?php

namespace App\Http\Middleware;

use Closure;
use App\Appointment;

class AppointmentsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->appointment;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'appointments.show' && in_array('show_appointments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'appointments.edit' && in_array('update_appointments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'appointments.update' && in_array('update_appointments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'appointments.destroy' && in_array('destroy_appointments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Appointment) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $appointment = Appointment::findOrFail($model);
            if (isset($appointment)) {
                $modelCreatorId = (int)$appointment->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
