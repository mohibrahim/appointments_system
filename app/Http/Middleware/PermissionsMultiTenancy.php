<?php

namespace App\Http\Middleware;

use Closure;
use App\Permission;

class PermissionsMultiTenancy
{
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->permission;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'permissions.show' && in_array('show_permissions_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'permissions.edit' && in_array('update_permissions_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'permissions.update' && in_array('update_permissions_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'permissions.destroy' && in_array('destroy_permissions_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Permission) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $permission = Permission::findOrFail($model);
            if (isset($permission)) {
                $modelCreatorId = (int)$permission->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
