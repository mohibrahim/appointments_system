<?php

namespace App\Http\Middleware;

use Closure;

class PermissionsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'permissions.index' && in_array('index_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.show' && in_array('show_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.create' && in_array('store_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.store' && in_array('store_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.edit' && in_array('update_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.update' && in_array('update_permissions', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'permissions.destroy' && in_array('destroy_permissions', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
