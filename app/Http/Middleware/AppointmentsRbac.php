<?php

namespace App\Http\Middleware;

use Closure;

class AppointmentsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'appointments.index' && in_array('index_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.show' && in_array('show_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.create' && in_array('store_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.store' && in_array('store_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.edit' && in_array('update_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.update' && in_array('update_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments.destroy' && in_array('destroy_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments_miscellaneous.index_search' && in_array('index_appointments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'appointments_miscellaneous.delete_personal_image_ajax' && in_array('update_appointments', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
