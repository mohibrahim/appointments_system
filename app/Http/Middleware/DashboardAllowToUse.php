<?php

namespace App\Http\Middleware;

use App\Services\Models\User\Dashboard\DashboardSpecifier;
use Closure;

class DashboardAllowToUse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, string $dashboardName)
    {
        $dashboardSpecifier = resolve(DashboardSpecifier::class);
        $isAllowed = $dashboardSpecifier->isAllowedForDashboard($dashboardName);
        if ($isAllowed) {
            return $next($request);
        }
        return abort(401);
    }
}
