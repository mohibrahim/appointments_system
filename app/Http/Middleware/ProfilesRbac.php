<?php

namespace App\Http\Middleware;

use Closure;

class ProfilesRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'profiles.index' && in_array('index_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.show' && in_array('show_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.create' && in_array('store_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.store' && in_array('store_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.edit' && in_array('update_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.update' && in_array('update_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles.destroy' && in_array('destroy_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles_miscellaneous.index_search' && in_array('index_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles_miscellaneous.delete_personal_image' && in_array('update_profiles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'profiles_miscellaneous.get_users_names_ids' && (in_array('store_profiles', $permissions) || in_array('update_profiles', $permissions))) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
