<?php

namespace App\Http\Middleware;

use Closure;
use App\Profile;

class ProfilesMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->profile;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'profiles.show' && in_array('show_profiles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'profiles.edit' && in_array('update_profiles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'profiles.update' && in_array('update_profiles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'profiles.destroy' && in_array('destroy_profiles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Profile) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $profile = Profile::findOrFail($model);
            if (isset($profile)) {
                $modelCreatorId = (int)$profile->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
