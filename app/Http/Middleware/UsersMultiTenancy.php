<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class UsersMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->user;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'users.show' && in_array('show_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'users.edit' && in_array('update_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'users.update' && in_array('update_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'users.destroy' && in_array('destroy_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'users_miscellaneous.delete_personal_image_ajax' && in_array('update_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'users_miscellaneous.delete_personal_image_without_verification_ajax' && in_array('update_users_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof User) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $user = User::findOrFail($model);
            if (isset($user)) {
                $modelCreatorId = (int)$user->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
