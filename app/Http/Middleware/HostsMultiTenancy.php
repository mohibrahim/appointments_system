<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class HostsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->host;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'hosts.show' && in_array('show_hosts_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'hosts.edit' && in_array('update_hosts_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'hosts.update' && in_array('update_hosts_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'hosts.destroy' && in_array('destroy_hosts_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof User) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $host = User::findOrFail($model);
            if (isset($host)) {
                $modelCreatorId = (int)$host->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
