<?php

namespace App\Http\Middleware;

use Closure;

class HostsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'hosts.index' && in_array('index_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.show' && in_array('show_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.create' && in_array('store_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.store' && in_array('store_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.edit' && in_array('update_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.update' && in_array('update_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts.destroy' && in_array('destroy_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts_miscellaneous.index_search' && in_array('index_hosts', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'hosts_miscellaneous.delete_personal_image_ajax' && in_array('update_hosts', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
