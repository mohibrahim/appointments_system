<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Services\FileUploader\FileUploaderInterface;
use App\Services\IndexTenancy\IndexStrategyInterface;
use App\Services\LanguageLocator\LanguageLocatorTrait;
use App\Services\WorldCountries\LanguageCountriesResolverInterface;
use App\Services\Models\Profile\CreateProfileStrategy\CreateProfileInterface;
use App\Services\Models\Profile\DeleteStrategy\DeleteProfileInterface;
use App\Services\Models\Profile\UpdateProfileStrategy\UpdateProfileInterface;

class ProfileController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('profiles_rbac');
        $this->middleware('profiles_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'profileSystem']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Profile $profile)
    {
        $authenticatedUser = request()->user();
        $builder = $indexByMultiTenancy->index($profile, $authenticatedUser, 'index_profiles_multi_tenancy');
        $profiles = $builder->paginate(20);
        return view('profiles.index', compact('profiles'));
    }

    public function create(LanguageCountriesResolverInterface $languageCountriesResolver, Profile $profile)
    {
        $countries = $languageCountriesResolver->getSuitableCountriesListByCurrentLanguage();
        return view('profiles.create', compact('countries', 'profile'));
    }


    public function store(
        ProfileRequest $request,
        CreateProfileInterface $createUser,
        FileUploaderInterface $fileUploader
    ) {
        $profile = $createUser->create($request, $fileUploader);
        flash()->success(__('profile.profile_added_successfully'));
        return redirect()->action('ProfileController@show', ['profile' => $profile->id]);
    }


    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        return view('profiles.show', compact('profile'));
    }

    public function edit($id, LanguageCountriesResolverInterface $languageCountriesResolver)
    {
        $profile = Profile::findOrFail($id);
        $countries = $languageCountriesResolver->getSuitableCountriesListByCurrentLanguage();
        return view('profiles.edit', compact('profile', 'countries'));
    }

    public function update(
        ProfileRequest $request,
        int $id,
        UpdateProfileInterface $updateProfile,
        FileUploaderInterface $fileUploader
    ) {
        $profile = $updateProfile->update($id, $request, $fileUploader);
        flash()->success(__('profile.profile_updated_successfully'))->important();
        return redirect()->action('ProfileController@show', ['profile' => $profile->id]);
    }

    public function destroy(
        Profile $profile,
        FileUploaderInterface $fileUploader,
        DeleteProfileInterface $deleteProfile
    ) {
        $deleteProfile->delete($profile, $fileUploader);
        flash()->success( __('profile.profile_deleted_successfully_segment_1') . $profile->name .  __('profile.profile_deleted_successfully_segment_1'))->important();
        return redirect('profiles');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, IndexStrategyInterface $indexStrategy, Profile $profile)
    {
        $authenticatedUser = request()->user();
        $builder = $indexStrategy->index($profile, $authenticatedUser, 'index_profiles_multi_tenancy');
        $results = $profile->search($builder, $request->input('keyword'));
        return $results;
    }

    public function deletePersonalImage(Request $request)
    {
        $personalImage = $request->input('personal_image_name');
        $profiles = Profile::where('personal_image', $personalImage)->get();
        if ($profiles->isNotEmpty()) {
            $profile = $profiles->first();
            $fileUploader = resolve(FileUploaderInterface::class);
            $fileUploader->setFileNameToSkip('no_image.png');
            $fileUploader->delete($profile->personal_image, 'images/profiles');
            $profile->personal_image = 'no_image.png';
            $profile->save();
            return response()->json(['process'=>'success'], 200);
        }
    }

}
