<?php

namespace App\Http\Controllers;

use App\Services\Models\User\Dashboard\DashboardSpecifier;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(DashboardSpecifier $dashboardSpecifier)
    {
        return $dashboardSpecifier->specifyUserDashboard();
    }
}
