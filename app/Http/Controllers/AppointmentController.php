<?php

namespace App\Http\Controllers;

use Closure;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentRequest;
use App\Services\IndexTenancy\IndexStrategyInterface;
use App\Services\LanguageLocator\LanguageLocatorTrait;
use App\Services\Models\Appointment\DeleteHostStrategy\DeleteAppointmentInterface;
use App\Services\Models\Appointment\CreateAppointmentStrategy\CreateAppointmentInterface;
use App\Services\Models\Appointment\UpdateAppointmentStrategy\UpdateAppointmentInterface;

class AppointmentController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('appointments_rbac');
        $this->middleware('appointments_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'appointments']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Appointment $appointment)
    {
        $authenticatedUser = request()->user();
        $builder = $indexByMultiTenancy->index($appointment, $authenticatedUser, 'index_appointments_multi_tenancy');
        $appointments = $builder->paginate(20);
        return view('appointments.index', compact('appointments'));
    }

    public function create(Appointment $appointment)
    {
        return view('appointments.create', compact('appointment'));
    }


    public function store(
        AppointmentRequest $request,
        CreateAppointmentInterface $createAppointment
    ) {
        $appointment = $createAppointment->create($request);
        flash()->success(__('appointment.appointment_added_successfully'));
        return redirect()->action('AppointmentController@show', ['appointment' => $appointment->id]);
    }


    public function show($id)
    {
        $appointment = Appointment::findOrFail($id);
        return view('appointments.show', compact('appointment'));
    }

    public function edit($id)
    {
        $appointment = Appointment::findOrFail($id);
        return view('appointments.edit', compact('appointment'));
    }

    public function update(
        AppointmentRequest $request,
        int $appointmentId,
        UpdateAppointmentInterface $updateAppointment
    ) {
        $appointment = $updateAppointment->update($appointmentId, $request);
        flash()->success(__('appointment.appointment_updated_successfully'))->important();
        return redirect()->action('AppointmentController@show', ['appointment' => $appointment->id]);
    }

    public function destroy(
        Appointment $appointment,
        DeleteAppointmentInterface $deleteAppointment
    ) {
        $deleteAppointment->delete($appointment);
        flash()->success(__('appointment.appointment_deleted_successfully_segment_1') . $appointment->title .  __('appointment.appointment_deleted_successfully_segment_1'))->important();
        return redirect('appointments');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, IndexStrategyInterface $indexStrategy, Appointment $appointment)
    {
        $authenticatedAppointment = request()->user();
        $builder = $indexStrategy->index($appointment, $authenticatedAppointment, 'index_appointments_multi_tenancy');
        $results = $appointment->search($builder, $request->input('keyword'));
        return $results;
    }
}
