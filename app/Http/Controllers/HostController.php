<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\HostRequest;
use App\Http\Controllers\Controller;
use App\Services\IndexTenancy\IndexStrategyInterface;
use App\Services\LanguageLocator\LanguageLocatorTrait;
use App\Services\Models\User\Host\CreateHostStrategy\CreateHostInterface;
use App\Services\Models\User\Host\DeleteHostStrategy\DeleteHostInterface;
use App\Services\Models\User\Host\UpdateHostStrategy\UpdateHostInterface;
use App\Services\Models\User\Type\UserType;

class HostController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('hosts_rbac');
        $this->middleware('hosts_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'hosts']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, User $host)
    {
        $authenticatedUser = request()->user();
        $builder = $indexByMultiTenancy->index($host, $authenticatedUser, 'index_hosts_multi_tenancy');
        $hosts = $builder->where('type', UserType::HOST)
            ->paginate(20);
        return view('users.hosts.index', compact('hosts'));
    }

    public function create(User $host)
    {
        return view('users.hosts.create', compact('host'));
    }


    public function store(
        HostRequest $request,
        CreateHostInterface $createHost
    ) {
        $host = $createHost->create($request);
        flash()->success(__('host.host_added_successfully'));
        return redirect()->action('HostController@show', ['host' => $host->id]);
    }


    public function show($hostId)
    {
        $host = User::where('id', $hostId)
            ->where('type', UserType::HOST)
            ->firstOrFail();
        return view('users.hosts.show', compact('host'));
    }

    public function edit($hostId)
    {
        $host = User::where('id', $hostId)
            ->where('type', UserType::HOST)
            ->firstOrFail();
        return view('users.hosts.edit', compact('host'));
    }

    public function update(
        HostRequest $request,
        int $hostId,
        UpdateHostInterface $updateHost
    ) {
        $host = $updateHost->update($hostId, $request);
        flash()->success(__('host.host_updated_successfully'))->important();
        return redirect()->action('HostController@show', ['host' => $host->id]);
    }

    public function destroy(
        User $host,
        DeleteHostInterface $deleteHost
    ) {
        $deleteHost->delete($host);
        flash()->success(__('host.host_deleted_successfully_segment_1') . $host->name .  __('host.host_deleted_successfully_segment_1'))->important();
        return redirect('hosts');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, IndexStrategyInterface $indexStrategy, User $host)
    {
        $authenticatedUser = request()->user();
        $builder = $indexStrategy->index($host, $authenticatedUser, 'index_hosts_multi_tenancy');
        $results = $host->searchForHost($builder, $request->input('keyword'));
        return $results;
    }
}
