<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Services\Models\User\Dashboard\DashboardSpecifier;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function specify(DashboardSpecifier $dashboardSpecifier)
    {
        return $dashboardSpecifier->specifyUserDashboard();
    }
}
