<?php

namespace App\Http\Controllers\Dashboard;

use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Models\User\Dashboard\DashboardControllerInterface;

class HostDashboardController extends Controller implements DashboardControllerInterface
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dashboard_allow_to_use:'.config('rbac.roles_names_dashboard_controllers_names')['Host']);
    }

    public function home()
    {
        $authenticatedUser = request()->user();
        $nonReservedAppointments = $authenticatedUser->hostAppointments()
            ->whereNull('attendee_id')
            ->get();
        $reservedAppointments = $authenticatedUser->hostAppointments()
            ->whereNotNull('attendee_id')
            ->get();
        return response()->view('dashboard.host', compact('nonReservedAppointments', 'reservedAppointments'));
    }


    public function joinAppointment(Request $request)
    {
        $appointmentId = $request->input('appointment_id');
        return response()->view('dashboard.host_room', compact('appointmentId'));
    }

    public function joinAppointmentGetSignature(Request $request)
    {
        $appointmentId = $request->input('appointmentId');
        $appointment = Appointment::findOrFail($appointmentId);
        $hostName = optional($appointment->host)->name;

        $apiKey = config('appointments_system.zoom_integration.api_key');
        $apiSecret = config('appointments_system.zoom_integration.secret_key');
        $meetingNumber = $appointment->zoom_meeting_number;
        $role = 1;


        $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
        $data = base64_encode($apiKey . $meetingNumber . $time . $role);
        $hash = hash_hmac('sha256', $data, $apiSecret, true);
        $_sig = $apiKey . "." . $meetingNumber . "." . $time . "." . $role . "." . base64_encode($hash);

        //return signature, url safe base64 encoded
        $signature = rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
        return [
            'meetingNumber' => $meetingNumber,
            'userName' => $hostName,
            'signature' => $signature,
            'apiKey' => $apiKey,
        ];
    }


}
