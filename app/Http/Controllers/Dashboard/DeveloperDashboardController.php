<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Services\Models\User\Dashboard\DashboardControllerInterface;

class DeveloperDashboardController extends Controller implements DashboardControllerInterface
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dashboard_allow_to_use:'.config('rbac.roles_names_dashboard_controllers_names')['Developer']);
    }

    public function home()
    {
        return response()->view('dashboard.developer');
    }


}
