<?php

namespace App\Http\Controllers\Dashboard;

use App\Appointment;
use App\Http\Controllers\Controller;
use App\Services\Models\User\Dashboard\DashboardControllerInterface;
use Illuminate\Http\Request;

class AttendeeDashboardController extends Controller implements DashboardControllerInterface
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dashboard_allow_to_use:'.config('rbac.roles_names_dashboard_controllers_names')['Attendee']);
    }

    public function home()
    {
        $authenticatedUser = request()->user();
        $nonReservedAppointments = Appointment::whereNull('attendee_id')
            ->whereNotNull('host_id')
            ->get();
        $reservedAppointments = $authenticatedUser->attendeeAppointments()
            ->where('attendee_id', $authenticatedUser->id)
            ->get();
        return response()->view('dashboard.attendee', compact('nonReservedAppointments', 'reservedAppointments'));
    }

    public function reserveAppointment(Request $request)
    {
        $attendee = $request->user();
        $appointmentId = $request->input('appointment_id');
        $appointment = Appointment::where('id', $appointmentId)
            ->whereNull('attendee_id')
            ->firstOrFail();
        $appointment->update(['attendee_id'=>$attendee->id]);
        return back();
    }

    public function joinAppointment(Request $request)
    {
        $appointmentId = $request->input('appointment_id');
        return response()->view('dashboard.attendee_room', compact('appointmentId'));
    }

    public function joinAppointmentGetSignature(Request $request)
    {
        $appointmentId = $request->input('appointmentId');
        $appointment = Appointment::findOrFail($appointmentId);
        $attendeeName = optional($appointment->attendee)->name;

        $apiKey = config('appointments_system.zoom_integration.api_key');
        $apiSecret = config('appointments_system.zoom_integration.secret_key');
        $meetingNumber = $appointment->zoom_meeting_number;
        $role = 0;


        $time = time() * 1000 - 30000;//time in milliseconds (or close enough)
        $data = base64_encode($apiKey . $meetingNumber . $time . $role);
        $hash = hash_hmac('sha256', $data, $apiSecret, true);
        $_sig = $apiKey . "." . $meetingNumber . "." . $time . "." . $role . "." . base64_encode($hash);

        //return signature, url safe base64 encoded
        $signature = rtrim(strtr(base64_encode($_sig), '+/', '-_'), '=');
        return [
            'meetingNumber' => $meetingNumber,
            'userName' => $attendeeName,
            'signature' => $signature,
            'apiKey' => $apiKey,
        ];
    }


}

