<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Services\FileUploader\FileUploaderInterface;
use App\Services\IndexTenancy\IndexStrategyInterface;
use App\Services\LanguageLocator\LanguageLocatorTrait;
use App\Services\Models\User\CreateUserStrategy\CreateUserInterface;
use App\Services\Models\User\DeleteUserStrategy\DeleteUserInterface;
use App\Services\Models\User\UpdateUserStrategy\UpdateUserInterface;

class UserController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('users_rbac');
        $this->middleware('users_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'userSystem']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = request()->user();
        $builder = $indexByMultiTenancy
            ->index($authenticatedUser, $authenticatedUser, 'index_users_multi_tenancy');
        $users = $builder->paginate(20);
        return view('users.index', compact('users'));
    }

    public function create(User $user)
    {
        return view('users.create', compact('user'));
    }


    public function store(
        UserRequest $request,
        CreateUserInterface $createUser
    ) {
        $user = $createUser->create($request);
        flash()->success(__('user.user_added_successfully'));
        return redirect()->action('UserController@show', ['user' => $user->id]);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    public function update(
        UserRequest $request,
        int $id,
        UpdateUserInterface $updateUser
    ) {
        $user = $updateUser->update($id, $request);
        flash()
            ->success(__('user.user_updated_successfully'))
            ->important();
        return redirect()->action('UserController@show', ['user' => $user->id]);
    }

    public function destroy(User $user, DeleteUserInterface $deleteUser, FileUploaderInterface $fileUploader)
    {
        $deleteUser->delete($user, $fileUploader);
        flash()
            ->success(__('user.user_deleted_successfully_segment_1') . __('user.user_deleted_successfully_segment_2'))
            ->important();
        return redirect('users');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function indexUsersNotVerifiedEmail()
    {
        $usersNotVerified = User::whereNull('email_verified_at')->paginate(25);
        return view('users.index_users_not_verified_email', compact('usersNotVerified'));
    }

    public function verifyUserEmail(User $user)
    {
        $user->update(['email_verified_at' => now()]);
        flash()->success('تم تفعيل اشتراك المستخدم: ' . $user->name)->important();
        return redirect()->action('UserController@indexUsersNotVerifiedEmail');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, IndexStrategyInterface $indexStrategy)
    {
        $authenticatedUser = request()->user();
        $builder = $indexStrategy
            ->index($authenticatedUser, $authenticatedUser, 'index_users_multi_tenancy');
        $results = $authenticatedUser->search($builder, $request->input('keyword'));
        return $results;
    }
}
