<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class LanguageController extends Controller
{

    public function switchLanguage($language)
    {
    	return back()->withHeaders([])->cookie('current_language', $language, 525600);
    }

}
