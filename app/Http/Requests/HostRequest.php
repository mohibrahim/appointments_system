<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
        ];
        $rules = $this->validatePassword($rules);
        return $rules;
    }

    private function validatePassword(array $rules): array
    {
        if ($this->route()->named('hosts.update')){
            $rules['password'] = ['nullable', 'string', 'min:8', 'confirmed'];
            return $rules;
        }
        $rules['password'] = ['required', 'string', 'min:8', 'confirmed'];
        return $rules;
    }

}
